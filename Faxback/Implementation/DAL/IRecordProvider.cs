﻿using System;
using System.Collections.Generic;

namespace FaxbackModel.Implementation
{
    public interface IRecordProvider<T>
    {
        IEnumerable<T> GetRecords(Func<T, bool> predicate = null);
        bool InsertOrUpdateRecords(IEnumerable<T> records);
        bool InsertOrUpdateRecord(T record);
    }
}