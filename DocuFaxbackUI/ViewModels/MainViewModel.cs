﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Caliburn.Micro;
using Faxback.Interface;
using FaxbackModel.Implementation.BusinessLogic;

namespace DocuFaxbackUI.ViewModels
{
    public class MainViewModel : Conductor<PropertyChangedBase>.Collection.AllActive
    {

        public MainViewModel(IEnumerable<FullAppCardViewModel> fullAppCards, IEnumerable<IObservableFaxProcess> processes, string title, int maxThreads = int.MaxValue, bool autostart = false)
        {
            this._manager = new ManagementLayer(maxThreads);
            processes.ToList().ForEach(i => this._manager.AddProcess(i));
            this.Processes = processes;
            this.FullAppCards = new BindableCollection<FullAppCardViewModel>(fullAppCards);
            FullAppCards.ToList().ForEach(ac => Items.Add(ac));
            this._title = title;
            if (autostart)
                Start();
        }

        public IEnumerable<IObservableFaxProcess> Processes { get; }
        public BindableCollection<FullAppCardViewModel> FullAppCards { get; set; }

        ManagementLayer _manager;

        public string Title
        {
            get => $"{this._title} ({FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).ProductVersion})";
        }

        bool _programRunning;
        bool ProgramRunning
        {
            get => _programRunning;
            set
            {
                _programRunning = value;
                NotifyOfPropertyChange(() => CanStart);
                NotifyOfPropertyChange(() => CanStop);
            }
        }

        bool _programStopping;
        bool ProgramStopping
        {
            get => _programStopping;
            set
            {
                _programStopping = value;
                NotifyOfPropertyChange(() => CanStop);
            }
        }

        bool _programWillExit;
        bool ProgramWillExit
        {
            get => _programWillExit;
            set
            {
                _programWillExit = value;
                NotifyOfPropertyChange(() => CanStart);
                NotifyOfPropertyChange(() => CanStop);
                NotifyOfPropertyChange(() => CanExit);
            }
        }

        private string _stopButton = "Stop";
        public string StopButton
        {
            get
            {
                return _stopButton;
            }
            set
            {
                _stopButton = value;
                NotifyOfPropertyChange(() => StopButton);
            }
        }

        private string _startButton = "Start";
        public string StartButton
        {
            get
            {
                return _startButton;
            }
            set
            {
                _startButton = value;
                NotifyOfPropertyChange(() => StartButton);
            }
        }

        private string _title;

        private string _exitButton = "Exit";
        public string ExitButton
        {
            get
            {
                return _exitButton;
            }
            set
            {
                _exitButton = value;
                NotifyOfPropertyChange(() => ExitButton);
            }
        }

        public bool CanStart
        {
            get => !ProgramRunning && !ProgramWillExit;
        }

        public void Start()
        {
            this.Processes.ToList().ForEach(p => p.EnableProcess());
            this.StopButton = "Stop";
            this.StartButton = "Running";
            this._manager.RunTasks();
            ProgramRunning = true;
        }

        public bool CanStop
        {
            get => !ProgramStopping && !ProgramWillExit;
        }

        public void Stop()
        {
            this.ProgramStopping                 = true;
            this.StopButton                      = "Stopping";
            this.StartButton                     = "Start";

            InterruptProcesses();
        }

        private void StartEnable()
        {
            this.StopButton = "Stop";
            foreach(var f in this.FullAppCards)
            {
                foreach(var a in f.AppCards)
                {
                    a.Batch = "None";
                }
            }
            this.ProgramRunning = false;
            this.ProgramStopping = false;
        }


        private Task InterruptProcesses()
        {
            this._manager.StopProcesses();
            return Task.Run(() => 
            this._manager.WaitAll())
                .ContinueWith(_ => 
                StartEnable());
        }

        public bool CanExit
        {
            get => !ProgramWillExit;
        }

        public void Exit()
        {
            this.ExitButton = "Exiting";
            ProgramWillExit = true;
            InterruptProcesses().ContinueWith(_ => 
            Environment.Exit(0));
        }
    }
}
